/**
 * ProjectController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const httpStatus = require("../services/httpStatus");
const { ObjectId } = require('mongodb');
const userController = require('../controllers/UserController');
const { pipeline } = require("nodemailer/lib/xoauth2");

module.exports = {
    create: async function (req, res) {
        try {
            const requiredFields = ['name', 'category', 'description'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const userId = req.user.id;
            const { name, category, description } = req.body;

            const createProject = await Project.create({ name, category, description, userCreate: userId, userIdJoin: [{ id: userId }] });

            return httpStatus.OK(res, 'Create project success', { data: createProject });
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    updateProject: async function (req, res) {
        try {
            const requiredFields = ['projectId', 'name', 'category', 'description', 'userIdJoin'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const { projectId, name, category, description, userIdJoin } = req.body;
            const userId = req.user.id;

            const updateProject = await Project.updateOne({ id: projectId, userCreate: userId }).set({
                name,
                category,
                description,
                userIdJoin: userIdJoin.map(item => ({ id: item }))
            });

            if (!updateProject) {
                return httpStatus.NotFoundException(res, 'Not found project');
            }

            return httpStatus.OK(res, 'Update project success', { data: updateProject });
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    deleteProject: async function (req, res) {
        try {
            const projectId = req.body.projectId;
            if (!projectId) {
                return httpStatus.BadRequestException(res, 'Missing projectId');
            }

            const userId = req.user.id;
            const deleteProject = await Project.destroyOne({ id: projectId, userCreate: userId });
            if (!deleteProject) {
                return httpStatus.NotFoundException(res, 'Not found project');
            }

            return httpStatus.OK(res, 'Delete project success', { data: deleteProject });
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    find: async function (req, res) {
        try {
            const userId = req.user.id;
            const data = await Project.find({ 'userIdJoin.id': userId }).meta({ enableExperimentalDeepTargets: true }).sort('createdAt DESC');;

            return httpStatus.OK(res, 'Find project success', { data: data });
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    findOne: async function (req, res) {
        try {
            const userId = req.user.id;
            const dbProject = Project.getDatastore().manager;
            const projectNative = dbProject.collection(Project.tableName);

            const { id, keyTask, userAssign } = req.query;
            const pipeline = [];

            if (keyTask) {
                pipeline.push({ $match: { 'name': new RegExp(keyTask, 'i') } });
            }

            if (userAssign) {
                const arrUserAssign = userAssign.split(',');
                pipeline.push({ $match: { 'assignees': { $in: arrUserAssign } } });
            }

            let data = await projectNative.aggregate([
                { $match: { _id: new ObjectId(id) } },
                {
                    $lookup: {
                        from: 'task',
                        localField: '_id',
                        foreignField: 'projectId',
                        pipeline: pipeline,
                        as: 'task',
                    }
                },

            ]).toArray();

            if (data.length === 0) {
                return httpStatus.NotFoundException(res, 'Not found project')
            }

            data = data[0];

            data.infoUser = await Promise.all(data.userIdJoin.map(item => (
                userController.getInfoUser(item.id)
            )))

            data.infoUser?.forEach(item => {
                if (item.avatar) item.avatar = `${sails.config.DOMAIN}/${item.avatar}`
                else item.avatar = null
            });

            const progress = [
                { title: 'BACKLOG', id: 'BACKLOG' },
                { title: 'SELECTED FOR DEVELOPMENT', id: 'SELECTED FOR DEVELOPMENT' },
                { title: 'IN PROGRESS', id: 'IN PROGRESS' },
                { title: 'DONE', id: 'DONE' }
            ];



            progress.forEach(element => {
                const task = data.task.filter(item => item.progessId == element.id);
                element.tasks = task.sort((a, b) => (a.position - b.position));
                element.numberTask = task.length;
            });

            const arrTotalTask = progress.flatMap(element => element.tasks.flatMap(item => item.assignees));

            const uniqueAssignees = [...new Set(arrTotalTask)];

            const infoUserAssign = await User.find({ id: uniqueAssignees });

            progress.forEach(element => {
                element.tasks.forEach(task => {
                    task.infoUserAssign = infoUserAssign
                        .filter(user => task.assignees.includes(user.id))
                        .map(user => user.avatar ? `${sails.config.DOMAIN}/${user.avatar}` : null);
                });
            });

            data.progress = progress;
            data.task = undefined;

            return httpStatus.OK(res, 'Find project success', { data: data });
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    addUserToProject: async function (req, res) {
        try {
            const arrEmail = req.body.arrEmail;
            const projectId = req.body.projectId;

            const requiredFields = ['arrEmail', 'projectId'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const invalidEmails = arrEmail.filter(email => !check.validateEmail(email).result);
            if (invalidEmails.length > 0) {
                const errorMessage = `Invalid emails: ${invalidEmails.join(', ')}`;
                return httpStatus.BadRequestException(res, errorMessage);
            }

            const dataProject = await Project.findOne({ id: projectId });
            if (!dataProject) {
                return httpStatus.NotFoundException(res, 'Not found project');
            }

            const responseDataFindFromUser = await User.find({ email: arrEmail });

            if (responseDataFindFromUser.length !== arrEmail.length) {
                return httpStatus.NotFoundException(res, 'Not found user');
            }

            const isValidEmail = arrEmail.every(element => responseDataFindFromUser.some(item => item.email === element));

            if (!isValidEmail) {
                const invalidEmails = arrEmail.filter(element => !responseDataFindFromUser.some(item => item.email === element));
                const errorMessage = `Invalid emails: ${invalidEmails.join(', ')}`;
                return httpStatus.BadRequestException(res, errorMessage);
            }

            const arrUserJoin = dataProject.userIdJoin;

            const arrUserFromClientSend = responseDataFindFromUser.map(item => ({ id: item.id }));

            const arrUserJoinNew = [...arrUserJoin, ...arrUserFromClientSend];

            await Project.updateOne({ id: projectId }).set({
                userIdJoin: arrUserJoinNew
            });

            return httpStatus.OK(res, 'Add user success');

        } catch (error) {

            return httpStatus.ErrorServerException(res, error.message);
        }
    }

};

