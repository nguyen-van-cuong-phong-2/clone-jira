/**
 * TaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { ObjectId } = require('mongodb');
const httpStatus = require('../services/httpStatus');

module.exports = {
    create: async function (req, res) {
        try {
            const requiredFields = ['name', 'priority', 'type', 'projectId'];
            const { name, description = '', assignees = [], priority, type, projectId } = req.body;
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const maxPosition = await Task.find({ progessId: 'BACKLOG' }).sort('position DESC').limit(1);

            const position = maxPosition[0]?.position + 1 || 1;

            await Task.create({
                name,
                description,
                assignees,
                priority,
                type,
                progessId: 'BACKLOG',
                position: position,
                projectId: new ObjectId(projectId)
            })
            return httpStatus.OK(res, 'Create issue success')
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    update: async function (req, res) {
        try {
            const { taskId, name, description = '', assignees = [], priority, type, progessId, position, estimate = '' } = req.body;

            const requiredFields = ['taskId', 'name', 'priority', 'type'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const dataTask = await Task.findOne({ id: taskId });
            if (!dataTask) {
                return httpStatus.NotFoundException(res, 'Not found task');
            }

            await Task.updateOne({ id: taskId })
                .set({
                    name,
                    description,
                    assignees,
                    priority,
                    type,
                    progessId,
                    position,
                    estimate
                })
            return httpStatus.OK(res, 'Update task success');
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    updateTask: async function (req, res) {
        try {
            const { progessId, position, taskId } = req.body;

            const requiredFields = ['taskId', 'progessId', 'position'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));

            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const updatedTask = await Task.updateOne({ id: taskId })
                .set({
                    position: position,
                    progessId: progessId
                });

            if (updatedTask) {
                return httpStatus.OK(res, 'Update task success');
            }
            return httpStatus.NotFoundException(res, 'Not found task');
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    deleteTask: async function (req, res) {
        try {
            const { taskId } = req.body;
            if (!taskId) {
                return httpStatus.BadRequestException(res, 'Missing taskId');
            }

            const deleteTask = await Task.destroyOne({ id: taskId });
            if (!deleteTask) {
                return httpStatus.NotFoundException(res, 'Not found task');
            }

            return httpStatus.OK(res,'Delete task success')
        } catch (error) {
            return httpStatus.ErrorServerException(res, error.message);
        }
    },
};

