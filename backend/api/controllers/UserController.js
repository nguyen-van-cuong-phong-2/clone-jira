/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { ObjectId } = require('mongodb');
const axios = require('axios');
const eventBus = require('../services/event');
const httpStatus = require('../services/httpStatus');
module.exports = {

    // đăng ký tài khoản
    create: async function (req, res) {
        try {
            const { email, name } = req.body;

            const requiredFields = ['email', 'name'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));
            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const checkValidEmail = check.validateEmail(email);
            if (!checkValidEmail.result) {
                return httpStatus.BadRequestException(res, checkValidEmail.message);
            }

            const checkExistEmail = await User.count({ email: email });
            if (checkExistEmail) {
                return httpStatus.ConflictException(res, 'Email đã tồn tại');
            }

            const password = authen.randomPassword();

            let avatar = '';
            const file = req.file('file');
            if (file) {
                avatar = await uploadFile(file)
            }

            await User.create({
                email: email,
                name: name,
                password: authen.hashPassword(password),
                avatar: avatar
            });

            sendMail({
                mail: email,
                content: `Mật khẩu của bạn là ${password}`,
                subject: `Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi`
            });

            return httpStatus.OK(res, 'Create account success, please check your email');
        } catch (error) {
            if (error.code == 'E_EXCEEDS_UPLOAD_LIMIT') {
                return httpStatus.BadRequestException(res, 'File tải lên không hợp lệ.');
            }
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    // đăng nhập
    login: async function (req, res) {
        try {
            const requiredFields =  ['email', 'password'];
            const missingFields = requiredFields.filter(field => !req.body.hasOwnProperty(field));
            if (missingFields.length > 0) {
                return httpStatus.BadRequestException(res, `Missing fields: ${missingFields.join(', ')}`);
            }

            const email = req.body.email;
            const checkAccount = await User.findOne({ email: email });

            if (!checkAccount) {
                return httpStatus.NotFoundException(res, 'Tài khoản hoặc mật khẩu không chính xác, vui lòng kiếm tra lại!');
            }

            const password = req.body.password;
            const isValidPassword = authen.comparePassword(password, checkAccount.password);
            if (!isValidPassword) {
                return httpStatus.BadRequestException(res, 'Tài khoản hoặc mật khẩu không chính xác, vui lòng kiếm tra lại!');
            }

            checkAccount.password = undefined;
            const token = await authen.createToken(checkAccount, '2h');
            const refreshToken = await authen.createToken(checkAccount, '15d');

            return httpStatus.OK(res, 'Login success', { token, refreshToken, account: checkAccount });
        } catch (error) {
            console.log("🚀 ~ error:", error)
            return httpStatus.ErrorServerException(res, error.message);
        }
    },

    // lấy thông tin user
    getInfoUser: async function (id) {
        try {
            const data = await User.findOne({ id: id });
            data.password = undefined
            return data;
        } catch (error) {
            console.log("🚀 ~ error:", error)
            return null
        }
    },

    // lấy thông tin user bằng email
    getInfoUserByEmail: async function (email) {
        try {
            const data = await User.findOne({ email: email });
            return data;
        } catch (error) {
            return null
        }
    },
};

