module.exports = {
    attributes: {
        // _id: {
        //     type: 'string',
        //     columnName: '_id',
        //     required: true,
        //     unique: true,
        //     autoIncrement: true,
        // },
        content: {
            type: 'string',
            required: true
        },
        taskId: {
            type: 'number',
            required: true
        },
    }
};
