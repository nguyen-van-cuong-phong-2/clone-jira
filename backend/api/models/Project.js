module.exports = {
    attributes: {
        // _id: {
        //     type: 'string',
        //     columnName: '_id',
        //     required: true,
        //     unique: true,
        //     defaultsTo: () => new ObjectId(),
        //     autoIncrement: true,
        // },
        name: {
            type: 'string',
            required: true
        },
        category: {
            type: 'string',
            required: true
        },
        description: {
            type: 'string',
            defaultsTo: ''
        },
        userIdJoin: {
            type: 'json',
            defaultsTo: []
        },
        userCreate: {
            type: 'string',
            required: true
        }
    }
};
