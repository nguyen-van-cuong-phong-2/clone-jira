module.exports = {
    attributes: {
        // _id: {
        //     type: 'string',
        //     columnName: '_id',
        //     required: true,
        //     unique: true,
        //     defaultsTo: () => new ObjectId(),
        //     autoIncrement: true,
        // },
        name: {
            type: 'string',
            required: true
        },
        description: {
            type: 'string',
            defaultsTo: ''
        },
        assignees: {
            type: 'json',
            defaultsTo: []
        },
        priority: {
            type: 'number',
            required: true
        },
        type: {
            type: 'number',
            required: true
        },
        progessId: {
            type: 'string',
            required: true
        },
        projectId: {
            type: 'ref',
            required: true,
            columnType: 'objectid'
        },
        position: {
            type: 'number',
            required: true
        },
        estimate: {
            type: 'string',
            defaultsTo:''
        }
    },
    schema: true
};
