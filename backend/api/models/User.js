module.exports = {
    attributes: {
        // _id: {
        //     type: 'string',
        //     columnName: '_id',
        //     required: true,
        //     unique: true,
        //     defaultsTo: () => new ObjectId(),
        //     autoIncrement: true,
        // },
        email: {
            type: 'string',
            required: true
        },
        password: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: true
        },
        avatar: {
            type: 'string',
            defaultsTo: ''
        },
    },
    schema: true
};
