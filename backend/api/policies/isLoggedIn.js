module.exports = async function (req, res, proceed) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) {
        return httpStatus.Unauthorized(res);
    }
    const payload = await authen.verifyJwt(token);
    if (payload) {
        req.user = payload.data;
        return proceed();
    }
    return httpStatus.Unauthorized(res);
};