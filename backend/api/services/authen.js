const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const httpStatus = require('./httpStatus');
const functions = {};
const saltRounds = 10;
functions.randomPassword = () => {
    return Math.floor(Math.random() * 1000000) + 1;
}

functions.hashPassword = (password) => {
    try {
        return bcrypt.hashSync(password.toString(), saltRounds);
    } catch (error) {
        return null;
    }
}

functions.comparePassword = (myPlaintextPassword, hash) => {
    return bcrypt.compareSync(myPlaintextPassword, hash);
}

functions.createToken = async (data, time) => {
    return jwt.sign({ data }, sails.config.PRIVATE_KEY, { expiresIn: time });
};

functions.verifyJwt = async (token) => {
    try {
        const data = await jwt.verify(token, sails.config.PRIVATE_KEY);
        return data
    } catch (error) {
        return false;
    }
}
module.exports = functions;