const check = {};
check.checkMissingInput = (dataInput, dataCheck) => {
    try {
        const arrKeyInput = Object.keys(dataInput);
        const len = dataCheck.length;
        for (let i = 0; i < len; i++) {
            const element = dataCheck[i];
            if (!arrKeyInput.includes(element)) {
                return { result: false, message: `Missing ${element}`, code: 400 }
            }
        }
        return { result: true }
    } catch (error) {
        return { result: false, message: 'Internal server error', code: 500 }
    }
}

check.validateEmail = (input) => {
    try {
        const gmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!gmailRegex.test(input)) {
            return { result: false, message: `Invalid email`, code: 400 }
        }
        return { result: true }
    } catch (error) {
        return { result: false, message: 'Internal server error', code: 500 }
    }
}

module.exports = check;