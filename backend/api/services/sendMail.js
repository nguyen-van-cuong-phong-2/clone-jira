var nodemailer = require('nodemailer');

module.exports = (data) => {
    const transporter = nodemailer.createTransport({
        host: sails.config.HOST_MAIL,
        port: sails.config.PORT_MAIL,
        auth: {
            user: sails.config.USER_MAIL,
            pass: sails.config.PASS_MAIL
        }
    });
    transporter.sendMail({
        from: '👻 <cuongnv@jitsinnovationlabs.com>',
        to: data.mail,
        subject: data.subject,
        html: data.content,
    }, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}





