
module.exports = async (file) => {
    return new Promise((resolve, reject) => {
        try {
            file.upload({
                dirname: require('path').resolve(sails.config.appPath, 'assets/avatar'),
                maxBytes: 10000000,
                saveAs: function (__newFileStream, cb) {
                    cb(null, __newFileStream.filename);
                }
            }, (err, uploadedFiles) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(`avatar/${uploadedFiles[0].filename}`);
                }
            });
        } catch (error) {
            reject(error);
        }
    });

}