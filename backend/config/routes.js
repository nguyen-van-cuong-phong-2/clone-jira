/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  // đăng kí tài khoản
  'POST /api/register': 'user/create',

  // đăng nhập
  'POST /api/login': 'user/login',

  // tạo project
  'POST /api/createProject': 'project/create',

  // cập nhật project
  'POST /api/updateProject': 'project/updateProject',

  // xoá project
  'POST /api/deleteProject': 'project/deleteProject',

  // tìm kiếm project
  'GET /api/findOneProject': 'project/findOne',

  // tạo task
  'POST /api/createTask': 'task/createTask',

  // chuyển vị trí task
  'POST /api/changePositionTask': 'task/updateTask',

  // cập nhật task
  'POST /api/updateTask': 'task/update',

  // lấy thông tin người dùng bằng email
  'GET /api/getInfoUserByEmail': 'user/getInfoUserByEmail',

  // loop
  'POST /api/addUserToProject': 'project/addUserToProject',

  // xoá task 
  'POST /api/deleteTask': 'task/deleteTask'
};
