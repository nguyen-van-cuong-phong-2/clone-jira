import { useForm } from "react-hook-form";
import { useMutation } from '@tanstack/react-query';
import { handleEroor, POST } from "../../common";
import { useQueryClient } from '@tanstack/react-query';
const CreateProject = () => {
    const queryClient = useQueryClient();

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const onSubmit = (data) => {
        mutationCreateProject.mutate(data)
    };
    const mutationCreateProject = useMutation({
        mutationFn: (data) => {
            return POST('/api/createProject', data)
        },
        onSuccess: () => {
            alert('Create project success!');
            queryClient.invalidateQueries({ queryKey: ['getProject'] })
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    return (<>
        <div className="w-full h-full  flex justify-center">
            <div className="w-1/2 justify-items-start mt-5">
                <div className="font-medium text-2xl text-gray-600">Create Project</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Name</div>
                        <input type="text" className={`border border-gray-400 rounded-md p-1 w-full outline-none px-2 ${errors.name && 'border-red-600'}`} {...register("name", { required: 'Name is required' })} />
                        {errors.name?.type === "required" && (
                            <p role="alert" className="text-red-600">{errors.name.message}</p>
                        )}
                    </div>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Description</div>
                        <textarea type="text" className="border border-gray-400 rounded-md p-1 w-full outline-none px-2 h-40" {...register("description")} />
                    </div>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Project Category</div>
                        <select className=" border border-gray-400 rounded-md p-1 w-full outline-none px-2" {...register("category")}>
                            <option value="Software">Software</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Bussiness">Bussiness</option>
                            <option value="Sales">Sales</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div>
                        <button className="border bg-green-600 p-3 rounded-2xl mt-2 text-white text-xl" type="submit">
                            Create
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </>)
}
export default CreateProject;