import { Modal } from 'antd';
import PropTypes from 'prop-types';
import { useMutation } from '@tanstack/react-query';
import { handleEroor, POST_JSON } from '../../../common';
import { useQueryClient } from '@tanstack/react-query';
import { Select } from 'antd';
import { useState } from 'react';


const ModalAddUserToProject = ({ isShowModal, setIsShowModal, id }) => {
    const queryClient = useQueryClient();

    const handleCancel = () => {
        setIsShowModal(false)
    }

    const [dataUserDidSelect, setDataUserDidSelect] = useState([]);

    const handleChange = (item) => {
        setDataUserDidSelect([...item])
    };

    const mutationAddUserToProject = useMutation({
        mutationFn: (data) => {
            return POST_JSON('/api/addUserToProject', data)
        },
        onSuccess: () => {
            alert('Add user success!')
            queryClient.invalidateQueries({ queryKey: ['getTask'] });
            setIsShowModal(false)
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const handleSubmit = async () => {
        mutationAddUserToProject.mutate({ arrEmail: dataUserDidSelect, projectId: id });
    }
    return (
        <>
            <Modal title="Add user" open={isShowModal} onCancel={handleCancel} footer={[]}>
                <div className='flex flex-col'>
                    <Select
                        mode="tags"
                        style={{ width: '100%' }}
                        placeholder="Email user:"
                        onChange={handleChange}
                    />
                    <div className='p-3 border rounded bg-green-400 text-white mt-2 w-20 self-center cursor-pointer'
                        onClick={handleSubmit}>Add</div>
                </div>
            </Modal>
        </>
    );
};
ModalAddUserToProject.propTypes = {
    isShowModal: PropTypes.bool.isRequired,
    setIsShowModal: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
};

export default ModalAddUserToProject;


