import { Modal } from 'antd';
import PropTypes from 'prop-types';
import { useMutation } from '@tanstack/react-query';
import { handleEroor, POST } from '../../../common';
import { useForm } from "react-hook-form";
import { useQueryClient } from '@tanstack/react-query';

const ModalCreateProject = ({ isShowModal, setIsShowModal, id }) => {
    const queryClient = useQueryClient();

    const handleCancel = () => {
        setIsShowModal(false)
    }

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const onSubmit = (data) => {
        mutationCreateTask.mutate(data)
    };

    const mutationCreateTask = useMutation({
        mutationFn: (data) => {
            const item = {
                projectId: id,
                ...data
            }
            return POST('/task', item)
        },
        onSuccess: () => {
            alert('Create task success');
            queryClient.invalidateQueries({ queryKey: ['getTask'] })
            handleCancel();
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    return (
        <>
            <Modal title="Create Issue" open={isShowModal}
                onCancel={handleCancel}
                footer={[

                ]}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div>
                        <div>Name:</div>
                        <input type="text" className='p-2 border rounded w-full outline-none focus:bg-slate-200' {...register('name', { required: "Name is required" })} />
                        {errors.name?.type === "required" && (
                            <p role="alert" className="text-red-600">{errors.name.message}</p>
                        )}
                    </div>
                    <div className='mt-2'>
                        <div>Description:</div>
                        <input type="text" className='p-2 border rounded w-full outline-none focus:bg-slate-200' {...register('description')}/>
                    </div>
                    <div className='mt-2'>
                        <div>Issue Type:</div>
                        <select className='w-full p-2 border rounded' {...register('type')}>
                            <option value="1">Task</option>
                            <option value="2">Bug</option>
                            <option value="3">Story</option>
                        </select>
                    </div>
                    <div className='mt-2'>
                        <div>Priority:</div>
                        <select className='w-full p-2 border rounded'{...register('priority')}>
                            <option value="1">Highest</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>
                        </select>
                    </div>
                    <div key={1} className='flex items-baseline justify-end mb-[10px] gap-2 mt-5'>
                        <div
                            className='border rounded-xl p-2 bg-red-600 text-white cursor-pointer'
                            onClick={handleCancel}
                        >Cancel</div>
                        <button
                            onClick={handleSubmit}
                            type='submit'
                            className='border rounded-xl p-2 bg-green-600 text-white cursor-pointer'
                        >Accept</button>
                    </div>
                </form>
            </Modal>
        </>
    );
};
ModalCreateProject.propTypes = {
    isShowModal: PropTypes.bool.isRequired,
    setIsShowModal: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
};

export default ModalCreateProject;


