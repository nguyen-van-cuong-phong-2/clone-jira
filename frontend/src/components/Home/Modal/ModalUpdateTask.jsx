import { Modal } from 'antd';
import PropTypes from 'prop-types';
import { useMutation } from '@tanstack/react-query';
import { handleEroor, POST_JSON } from '../../../common';
import { useForm } from "react-hook-form";
import { useQueryClient } from '@tanstack/react-query';
import { useState } from 'react';
import { Select } from 'antd';
const ModalUpdateTask = ({ isShowModal, setIsShowModal, dataTask, setIdTaskChoose, dataUserJoin }) => {
    const [userSelected, setUserSelected] = useState();
    const queryClient = useQueryClient();

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset
    } = useForm({
        defaultValues: {
            name: dataTask?.name,
            description: dataTask?.description
        }
    });

    const handleCancel = () => {
        reset()
        setIdTaskChoose(0)
        setIsShowModal(false)
    }

    const handleChange = (value) => {
        setUserSelected(value)
    };

    const options = [];
    for (let i = 0; i < dataUserJoin.length; i++) {
        const element = dataUserJoin[i];
        options.push({
            value: element.id,
            label: element.name,
        });
    }

    const defaultValueOptions = [];
    for (let i = 0; i < dataTask?.assignees?.length; i++) {
        const element = dataTask?.assignees[i];
        defaultValueOptions.push(element);
    }

    const onSubmit = (data) => {
        reset()
        mutationUpdateTask.mutate({
            ...data,
            assignees: userSelected
        })
    };

    const mutationUpdateTask = useMutation({
        mutationFn: (data) => {
            const item = {
                taskId: dataTask.id,
                ...data
            }
            return POST_JSON('/api/updateTask', item)
        },
        onSuccess: () => {
            alert('Update task success');
            queryClient.invalidateQueries({ queryKey: ['getTask'] })
            handleCancel();
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const mutationDeleteTask = useMutation({
        mutationFn: (data) => {
            const item = {
                taskId: dataTask.id,
                ...data
            }
            return POST_JSON('/api/deleteTask', item)
        },
        onSuccess: () => {
            alert('Delete task success');
            queryClient.invalidateQueries({ queryKey: ['getTask'] })
            handleCancel();
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const handleDeleteTask = () => {
        mutationDeleteTask.mutate({ taskId: dataTask.id })
    }
    return (
        <>
            <Modal title="Issue" open={isShowModal}
                onCancel={handleCancel}
                footer={[

                ]}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div>
                        <div>Name:</div>
                        <input type="text" className='p-2 border rounded w-full outline-none focus:bg-slate-200' defaultValue={dataTask?.name}   {...register('name', { required: "Name is required" })} />
                        {errors.name?.type === "required" && (
                            <p role="alert" className="text-red-600">{errors.name.message}</p>
                        )}
                    </div>
                    <div className='mt-2'>
                        <div>Description:</div>
                        <input type="text" className='p-2 border rounded w-full outline-none focus:bg-slate-200' defaultValue={dataTask?.description} {...register('description')} />
                    </div>
                    <div className='mt-2'>
                        <div>Issue Type:</div>
                        <select className='w-full p-2 border rounded' {...register('type')}>
                            <option value="1" defaultChecked={dataTask?.type == 1}>Task</option>
                            <option value="2" defaultChecked={dataTask?.type == 2}>Bug</option>
                            <option value="3" defaultChecked={dataTask?.type == 3}>Story</option>
                        </select>
                    </div>
                    <div className='mt-2'>
                        <div>Assign:</div>
                        <Select
                            mode="tags"
                            style={{
                                width: '100%',
                            }}
                            placeholder="Tags Mode"
                            onChange={handleChange}
                            options={options}
                            defaultValue={defaultValueOptions}
                        />                    </div>
                    <div className='mt-2'>
                        <div>Priority:</div>
                        <select className='w-full p-2 border rounded'{...register('priority')}>
                            <option value="1">Highest</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>
                        </select>
                    </div>
                    <div key={1} className='flex items-baseline justify-end mb-[10px] gap-2 mt-5'>
                        <div
                            className='border rounded-xl p-2 bg-red-600 text-white cursor-pointer'
                            onClick={handleDeleteTask}
                        >Delete</div>
                        <button
                            onClick={handleSubmit}
                            type='submit'
                            className='border rounded-xl p-2 bg-green-600 text-white cursor-pointer'
                        >Update</button>
                    </div>
                </form>
            </Modal>
        </>
    );
};
ModalUpdateTask.propTypes = {
    isShowModal: PropTypes.bool.isRequired,
    setIsShowModal: PropTypes.func.isRequired,
    dataTask: PropTypes.object.isRequired,
    setIdTaskChoose: PropTypes.func.isRequired,
    dataUserJoin: PropTypes.array.isRequired,
};

export default ModalUpdateTask;


