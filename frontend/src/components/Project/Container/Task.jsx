import PropTypes from 'prop-types';
import { RiTaskFill } from "react-icons/ri";
import { FaBug } from "react-icons/fa6";
import { TbBrandStorytel } from "react-icons/tb";
import { FaLongArrowAltUp } from "react-icons/fa";
import { FaArrowDownLong } from "react-icons/fa6";
import { Draggable } from 'react-beautiful-dnd';
const Task = ({ task, index, setIdTaskChoose }) => {

    return (<Draggable draggableId={task._id} index={index}>
        {(provided) => (
            <div className="w-full border rounded-md p-2 bg-white shadow-lg mt-1"
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                ref={provided.innerRef}
                onClick={() => {
                    setIdTaskChoose(task._id);
                   
                }}
            >
                <div>{task.name}</div>
                <div className='flex justify-between items-center mt-2'>
                    <div className='flex'>
                        {task?.type == 1 && <RiTaskFill className='text-blue-500'></RiTaskFill>}
                        {task?.type == 2 && <FaBug className='text-red-500'></FaBug>}
                        {task?.type == 3 && <TbBrandStorytel className='text-green-500'></TbBrandStorytel>}
                        {task?.priority == 1 && <FaLongArrowAltUp className='text-red-500'></FaLongArrowAltUp>}
                        {task?.priority == 2 && <FaLongArrowAltUp className='text-red-300'></FaLongArrowAltUp>}
                        {task?.priority == 3 && <FaArrowDownLong className='text-green-500'></FaArrowDownLong>}
                    </div>
                    <div className='flex'>
                        {
                            task?.infoUserAssign?.map((item, index) => (
                                <>
                                    <div key={index} className="w-7 h-7 ml-[-5px] cursor-pointer">
                                        <img src={item} alt="avatar" className="w-full h-full object-cover rounded-full" />
                                    </div>
                                </>
                            ))
                        }
                    </div>
                </div>

            </div>
        )}
    </Draggable>)
}
Task.propTypes = {
    index: PropTypes.number.isRequired,
    task: PropTypes.object.isRequired,
    setIdTaskChoose: PropTypes.func.isRequired,
    setIsShowModal: PropTypes.func.isRequired,
}
export default Task;