import PropTypes from 'prop-types';
import Task from './Task';
import { Droppable } from 'react-beautiful-dnd';
import { useEffect, useState } from 'react';
import ModalUpdateTask from '../../Home/Modal/ModalUpdateTask';
import { GET } from '../../../common';

const Container = ({ title, numberTask, tasks, id, dataUserJoin }) => {
    const [isShowModal, setIsShowModal] = useState(false);
    const [idTaskChoose, setIdTaskChoose] = useState(null);
    const [dataTask, setDataTask] = useState();
    useEffect(() => {
        if (idTaskChoose) {
            const handleCallApiGetDataTask = async () => {
                const response = await GET(`/task/${idTaskChoose}`);
                if (response.status === 200) {
                    setDataTask(response.data);
                    setIsShowModal(true)
                }
            }
            handleCallApiGetDataTask();
        }

    }, [idTaskChoose])

    return (
        <>
            <div className='w-1/4 bg-[#F4F5F7] rounded p-2'>
                <div className='text-base text-gray-500'>{title} {numberTask}</div>
                <div className='mt-2'>
                    <Droppable droppableId={id}>
                        {
                            (provided) => (
                                <div ref={provided.innerRef} {...provided.droppableProps} style={{ minHeight: '200px' }}>
                                    {
                                        tasks?.map((item, index) => (
                                            <Task key={item.id} index={index} task={item} setIdTaskChoose={setIdTaskChoose} setIsShowModal={setIsShowModal}></Task>

                                        ))
                                    }
                                    {provided.placeholder}
                                </div>
                            )
                        }
                    </Droppable>
                </div>
                <ModalUpdateTask dataUserJoin={dataUserJoin} isShowModal={isShowModal} setIsShowModal={setIsShowModal} dataTask={dataTask} setIdTaskChoose={setIdTaskChoose}></ModalUpdateTask>
            </div >
        </>
    )
}

Container.propTypes = {
    title: PropTypes.string.isRequired,
    numberTask: PropTypes.number.isRequired,
    tasks: PropTypes.array.isRequired,
    id: PropTypes.string.isRequired,
    dataUserJoin: PropTypes.array.isRequired
}
export default Container;