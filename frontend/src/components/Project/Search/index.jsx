import { CiSearch } from "react-icons/ci";
import { IoPersonAdd } from "react-icons/io5";
import { CiSettings } from "react-icons/ci";
import PropTypes from 'prop-types';
import ModalAddUserToProject from "../../Home/Modal/ModalAddUserToProject";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Search = ({ setSearchKey, infoUser, id, setFilterByUser, filterByUser }) => {
    const [isShowModal, setIsShowModal] = useState(false);
    const navigate = useNavigate();

    const handleClickSearchFollowUser = (id) => {
        if (filterByUser.includes(id)) {
            setFilterByUser(prev => prev.filter(item => item != id))
        } else {
            setFilterByUser(prev => [...prev, id])
        }
    }
    return (<>
        <div className="w-[90%] mt-5">
            <div className="relative flex gap-2 items-center">
                <div className="absolute top-2 left-2">
                    <CiSearch className="text-xl text-gray-700"></CiSearch>
                </div>
                <input type="text" className="border-gray-400 outline-none p-1 px-10 w-1/4 border-2 rounded-md" onChange={(e) => setSearchKey(e.target.value)} />
                <div className="flex ml-2 items-center z-50 relative">
                    {

                        infoUser
                            ?.map((item, index) => (
                                <>
                                    <div key={index} className={`w-10 h-10 ml-[-5px] cursor-pointer  ${filterByUser.includes(item.id) ? '' : ''}`} onClick={() =>
                                        handleClickSearchFollowUser(item.id)}>
                                        <img src={item.avatar} alt="avatar" className={`w-full h-full object-cover rounded-full  ${filterByUser.includes(item.id) ? 'border-blue-500 border-4' : ''}`} />
                                    </div >
                                </>
                            ))
                    }
                    <IoPersonAdd className="w-10 h-10 text-gray-700 ml-2 cursor-pointer" onClick={() => setIsShowModal(true)} />
                    <CiSettings className="w-10 ml-2 h-10 text-gray-700 cursor-pointer" onClick={() => navigate(`/SettingProject/${id}`)}></CiSettings>
                </div>
                <ModalAddUserToProject isShowModal={isShowModal} setIsShowModal={setIsShowModal} id={id}></ModalAddUserToProject>
            </div>

        </div >
    </>)
}
Search.propTypes = {
    setSearchKey: PropTypes.func.isRequired,
    infoUser: PropTypes.array.isRequired,
    id: PropTypes.string.isRequired,
    setFilterByUser: PropTypes.func.isRequired,
    filterByUser: PropTypes.array.isRequired,
}
export default Search;