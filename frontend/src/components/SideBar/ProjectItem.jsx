import PropTypes from 'prop-types';
import { FiCreditCard } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

const ProjectItem = ({ item }) => {
    const navigate = useNavigate();
    return (<>
        <div className='text-blue-700 text-base border-none rounded flex items-center gap-5 mt-3 py-2 px-6 box-border font-semibold hover:bg-slate-200 cursor-pointer' onClick={() => navigate(`/Project/${item.id}`)}>
            <FiCreditCard className='text-xl'></FiCreditCard>
            {item.name}
        </div>
    </>)
}
ProjectItem.propTypes = {
    item: PropTypes.object.isRequired
}
export default ProjectItem;