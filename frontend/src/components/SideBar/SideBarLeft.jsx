import { GoPlus } from "react-icons/go";
import { CiLogout } from "react-icons/ci";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { logOut } from "../../store/Slices/userSlice";

const SideBarLeft = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    return (<>
        <div className="bg-[#0747a6] h-full w-[64px] hover:w-[200px] transition-[width] duration-500 ease-in-out z-50 absolute flex flex-col gap-5 overflow-hidden " >
            <div className="logo ml-5 mt-5 flex gap-5 items-center">
                <div className="w-max">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.76 75.76" width={28}>
                        <defs>
                            <linearGradient
                                id="linear-gradient"
                                x1="34.64"
                                y1="15.35"
                                x2={19}
                                y2="30.99"
                                gradientUnits="userSpaceOnUse"
                            >
                                <stop offset="0.18" stopColor="rgba(0, 82, 204, 0.2)" />
                                <stop offset={1} stopColor="#DEEBFE" />
                            </linearGradient>
                            <linearGradient
                                id="linear-gradient-2"
                                x1="38.78"
                                y1="60.28"
                                x2="54.39"
                                y2="44.67"
                                xlinkHref="#linear-gradient"
                            />
                        </defs>
                        <title>Jira Software-blue</title>
                        <g id="Layer_2" data-name="Layer 2">
                            <g id="Blue">
                                <path
                                    d="M72.4,35.76,39.8,3.16,36.64,0h0L12.1,24.54h0L.88,35.76A3,3,0,0,0,.88,40L23.3,62.42,36.64,75.76,61.18,51.22l.38-.38L72.4,40A3,3,0,0,0,72.4,35.76ZM36.64,49.08l-11.2-11.2,11.2-11.2,11.2,11.2Z"
                                    style={{ fill: "rgb(222, 235, 254)" }}
                                />
                                <path
                                    d="M36.64,26.68A18.86,18.86,0,0,1,36.56.09L12.05,24.59,25.39,37.93,36.64,26.68Z"
                                    style={{ fill: 'url("#linear-gradient")' }}
                                />
                                <path
                                    d="M47.87,37.85,36.64,49.08a18.86,18.86,0,0,1,0,26.68h0L61.21,51.19Z"
                                    style={{ fill: 'url("#linear-gradient-2")' }}
                                />
                            </g>
                        </g>
                    </svg>
                </div>

                <div className="text-white text-3xl overflow-hidden whitespace-nowrap">Jira</div>
            </div>
            <div className="flex items-center gap-5 hover:bg-[#deebff] py-2 cursor-pointer" onClick={() => navigate("/CreateProject")}>
                <div className="w-max ml-5">
                    <GoPlus className="text-white text-3xl" />
                </div>
                <div className="text-white font-semibold text-base overflow-hidden whitespace-nowrap" >
                    Create Project
                </div>
            </div>

            {/* <ModalCreateProject isShowModal={isShowPopupCreateProject} setIsShowModal={setIsShowPopupCreateProject}></ModalCreateProject> */}

            <div className="w-full flex items-center gap-5 hover:bg-[#deebff] cursor-pointer py-2 z-10 absolute bottom-5"
                onClick={() => {
                    Cookies.remove('token_jira');
                    dispatch(logOut());
                    navigate("/Login");
                }}
            >
                <div className="w-max ml-5">
                    <CiLogout className="text-white text-3xl"></CiLogout>
                </div>
                <div className="text-white font-semibold text-base overflow-hidden whitespace-nowrap">Log out</div>
            </div>
        </div>
    </>)
}
export default SideBarLeft;