import SideBarLeft from './SideBarLeft';
import SideBarRight from './SideBarRight';
const SideBar = () => {
    return (<>
        <div className='flex h-full w-full'>
            <SideBarLeft></SideBarLeft>
            <div className='ml-[64px] w-full h-full overflow-hidden'>
                <SideBarRight></SideBarRight>
            </div>
        </div>
    </>)
}
export default SideBar;