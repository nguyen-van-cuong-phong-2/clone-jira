import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { handleEroor, POST } from "../../common";
import { useMutation } from '@tanstack/react-query';
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { login } from "../../store/Slices/userSlice";

export default function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onSubmit = (data) => {
    mutationLogin.mutate(data)
  };

  const mutationLogin = useMutation({
    mutationFn: (data) => {
      return POST('/api/Login', data)
    },
    onSuccess: (data) => {
      alert('Login success!');
      Cookies.set('token_jira', data.data.token);
      dispatch(login());
      navigate("/");
    },
    onError: (error) => {
      handleEroor(error);
    },
  });

  return (
    <>
      <div className="w-screen h-screen flex justify-center items-center bg-blue-400">
        <form className="w-[30%] border rounded-2xl p-5 flex gap-2 flex-col border-slate-400 bg-white" onSubmit={handleSubmit(onSubmit)}>
          <div className="text-2xl text-blue-500 font-bold">Jira</div>
          <div className="input">
            <label htmlFor="Email" className="text-gray-500 text-sm ">Email:</label>
            <input type="email" className={`border outline-none w-full rounded-xl p-2 font-normal ${errors.email && 'border-red-600'}`} {...register("email", { required: "Email is required", pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ })} />
            {errors.email?.type === "required" && (
              <p role="alert" className="text-red-600">{errors.email.message}</p>
            )}
            {errors.email?.type === "pattern" && (
              <p role="alert" className="text-red-600">Invalid email</p>
            )}
          </div>
          <div className="input" >
            <label htmlFor="Email" className="text-gray-500 text-sm">Password:</label>
            <input type="password" className="border outline-none w-full rounded-xl p-2 " {...register("password", { required: true })} />
            {errors.email?.type === "required" && (
              <p role="alert" className="text-red-600">Password is required</p>
            )}
          </div>
          <div className="p-2 border w-max rounded-xl bg-green-500">
            <button type="submit" className="cursor-pointer font-semibold  w-full text-white">Login</button>
          </div>
          <div>
            You don't have an account? <Link to={'/Register'} className="text-blue-500 font-semibold">Register</Link>
          </div>
        </form>
      </div>
    </>
  )
}