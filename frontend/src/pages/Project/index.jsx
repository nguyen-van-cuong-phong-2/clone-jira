import { useEffect, useState } from "react";
import Container from "../../components/Project/Container";
import Search from "../../components/Project/Search";
import { DragDropContext } from 'react-beautiful-dnd';
import { GET, handleEroor, POST } from "../../common";
import ModalCreateTask from '../../components/Home/Modal/ModalCreateProject';
import { useParams } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useQueryClient } from '@tanstack/react-query';
import { useMutation } from '@tanstack/react-query';

const Project = () => {
    let { id } = useParams();
    const queryClient = useQueryClient();
    const [searchKey, setSearchKey] = useState('');
    const [isShowModal, setIsShowModal] = useState(false);
    const [filterByUser, setFilterByUser] = useState([]);

    // const queryWithoutSearchKey = {
    //     queryKey: ['getTask'], queryFn: async () => {
    //         const response = await GET(`/api/findOneProject?id=${id}`);
    //         return response.data.data
    //     }
    // }

    const query = {
        queryKey: ['getTask'], queryFn: async () => {
            let url = `/api/findOneProject?id=${id}`;
            if (searchKey) url += `&keyTask=${searchKey}`;
            if (filterByUser) {
                const userAssign = filterByUser.join(',');
                url += `&userAssign=${userAssign}`;
            }
            const response = await GET(url);
            return response.data.data
        }
    }

    const { data, isLoading } = useQuery(query);

    useEffect(() => {
        queryClient.invalidateQueries({ queryKey: ['getTask'] })
    }, [queryClient, searchKey, filterByUser])

    const mutationRegister = useMutation({
        mutationFn: (data) => {
            return POST('/api/changePositionTask', data)
        },
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['getTask'] })
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const onDragEnd = result => {
        const { destination, source } = result;

        if (!destination) {
            return;
        }

        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }

        const dataChange = [...data.progress];
        let taskChange = null;

        dataChange.find(item => {
            if (item.id == source.droppableId) {
                taskChange = item.tasks[source.index];
                return true;
            }
        });

        dataChange.forEach(item => {
            if (item.id == source.droppableId) {
                item.tasks.splice(source.index, 1)
            }
            if (item.id == destination.droppableId) {
                item.tasks.splice(destination.index, 0, taskChange)
            }
        });

        mutationRegister.mutate({
            taskId: taskChange._id,
            progessId: destination.droppableId,
            position: destination.index
        });
    };

    useEffect(() => {
        queryClient.invalidateQueries({ queryKey: ['getTask'] })
    }, [id, queryClient])


    if (isLoading) {
        return <div className="flex justify-center items-center h-full">Loading...</div>
    }
    return (<>
        <div className="w-full h-full px-8 py-5 overflow-y-auto">
            <div className="title font-semibold text-2xl">{data?.name}</div>
            <div className="flex justify-center  items-center">
                <Search setSearchKey={setSearchKey} infoUser={data?.infoUser} id={id} setFilterByUser={setFilterByUser} filterByUser={filterByUser}></Search>
                <div className="p-2 border rounded-xl bg-slate-400 text-white cursor-pointer"
                    onClick={() => setIsShowModal(true)}
                >Create Issue</div>
            </div>
            <div className="flex gap-5 mt-5 min-h-[500px]">
                <DragDropContext onDragEnd={onDragEnd}>
                    {
                        data?.progress?.map((item, index) => (
                            <Container key={index} title={item.title} numberTask={item.numberTask} id={item.id} tasks={item.tasks} dataUserJoin={data?.infoUser}></Container>
                        ))
                    }
                </DragDropContext>
            </div>
            <ModalCreateTask isShowModal={isShowModal} setIsShowModal={setIsShowModal} id={data?._id}></ModalCreateTask>
        </div >
    </>)
}
export default Project;