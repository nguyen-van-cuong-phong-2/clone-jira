import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { handleEroor, POST } from "../../common";
import { useMutation } from '@tanstack/react-query';
import { useState } from "react";
import { useNavigate } from "react-router-dom";
export default function Register() {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    
    const navigate = useNavigate();
    const [avatar, setAvatar] = useState();

    const onSubmit = (data) => {
        mutationRegister.mutate({ ...data, file: avatar.file })
    };

    const mutationRegister = useMutation({
        mutationFn: (data) => {
            return POST('/api/register', data)
        },
        onSuccess: () => {
            alert('Tạo tài khoản thành công vui lòng kiểm tra email của bạn!');
            return navigate("/Login");
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const handleChangeFile = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = () => {
                setAvatar({
                    file: file,
                    show: reader.result
                });
            };
            reader.readAsDataURL(file);
        }
    };

    return (
        <>
            <div className="w-screen h-screen flex justify-center items-center bg-blue-400">
                <form className="w-[30%] border rounded-2xl p-5 flex gap-2 flex-col border-slate-400 bg-white" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-2xl text-blue-500 font-bold">Jira</div>
                    <div className="input">
                        <label htmlFor="Email" className="text-gray-500 text-sm ">Email:</label>
                        <input type="email" className={`border outline-none w-full rounded-xl p-2 font-normal ${errors.email && 'border-red-600'}`} {...register("email", { required: "Email is required", pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ })} />
                        {errors.email?.type === "required" && (
                            <p role="alert" className="text-red-600">{errors.email.message}</p>
                        )}
                        {errors.email?.type === "pattern" && (
                            <p role="alert" className="text-red-600">Invalid email</p>
                        )}
                    </div>
                    <div className="input">
                        <label htmlFor="Email" className="text-gray-500 text-sm ">Name:</label>
                        <input type="text" className={`border outline-none w-full rounded-xl p-2 font-normal ${errors.name && 'border-red-600'}`} {...register("name", { required: "Name is required" })} />
                        {
                            errors.name?.type === "required" && (
                                <p role="alert" className="text-red-600">{errors.name.message}</p>
                            )
                        }
                    </div>

                    <div className="flex items-center cursor-pointer w-full h-max relative">
                        <div className='w-16 h-16 border rounded-full '>
                            <img alt="avatar" src={avatar ? avatar.show : "/user.jpg"} className='w-full h-full border rounded-full'></img>
                        </div>
                        <div className="text-sm">Choose another avatar</div>
                        <input type='file' className='opacity-0 w-full h-full z-50 absolute left-0 cursor-pointer' onChange={handleChangeFile}>
                        </input>
                    </div>

                    <div className="p-2 border w-max rounded-xl bg-green-500 self-center">
                        <button className="cursor-pointer font-semibold  w-full text-white" type="submit">Register</button>
                    </div>
                    <div>
                        Do you have already an account? <Link to={'/Login'} className="text-blue-500 font-semibold">Login</Link>
                    </div>
                </form>
            </div>
        </>
    )
}