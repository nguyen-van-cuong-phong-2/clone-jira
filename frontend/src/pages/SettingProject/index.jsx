import { useForm } from "react-hook-form";
import { useMutation } from '@tanstack/react-query';
import { GET, handleEroor, POST_JSON } from "../../common";
import { useParams } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';
import { useQuery } from '@tanstack/react-query';
import { Select } from 'antd';
import { useState } from "react";

const SettingProject = () => {
    let { id } = useParams();

    const queryClient = useQueryClient();
    const [userJoinProject, setUserJoinProject] = useState([]);

    const onSubmit = (data) => {
        const userIdJoin = userJoinProject.length > 0 ? userJoinProject : defaultValue;
        mutationUpdateProject.mutate({ ...data, projectId: id, userIdJoin })
    };

    const mutationUpdateProject = useMutation({
        mutationFn: (data) => {
            return POST_JSON('/api/updateProject', data)
        },
        onSuccess: () => {
            alert('Update project success!');
            queryClient.invalidateQueries(['getProject'])
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    const query = {
        queryKey: ['getTask'], queryFn: async () => {
            let url = `/api/findOneProject?id=${id}`;
            const response = await GET(url);
            return response.data.data
        }
    }

    const { data, isLoading, isError } = useQuery(query);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            name: data?.name,
            description: data?.description
        }
    });

    const options = [];
    const defaultValue = [];

    for (let i = 0; i < data?.infoUser.length; i++) {
        const element = data?.infoUser[i]
        options.push({
            value: element.id,
            label: element.name,
        });
        defaultValue.push(element.id)
    }

    const handleChange = (value) => {
        setUserJoinProject(value)
    };

    const handleDeleteProject = () => {
        mutationDeleteProject.mutate({ projectId: id })
    }

    const mutationDeleteProject = useMutation({
        mutationFn: (data) => {
            return POST_JSON('/api/deleteProject', data)
        },
        onSuccess: () => {
            alert('Delete project success!');
            queryClient.invalidateQueries(['getProject'])
        },
        onError: (error) => {
            handleEroor(error);
        },
    });

    if (isLoading) {
        return <div className="flex justify-center items-center h-full">Loading...</div>
    }

    if (isError) {
        return <div className="flex justify-center items-center h-full">Không tìm thấy project</div>
    }
    return (<>
        <div className="w-full h-full  flex justify-center">
            <div className="w-1/2 justify-items-start mt-5">
                <div className="font-medium text-2xl text-gray-600">Update Project - {data?.name}</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Name</div>
                        <input type="text" defaultValue={data?.name} className={`border border-gray-400 rounded-md p-1 w-full outline-none px-2 ${errors.name && 'border-red-600'}`} {...register("name", { required: 'Name is required' })} />
                        {errors.name?.type === "required" && (
                            <p role="alert" className="text-red-600">{errors.name.message}</p>
                        )}
                    </div>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Description</div>
                        <textarea type="text" defaultValue={data?.description} className="border border-gray-400 rounded-md p-1 w-full outline-none px-2 h-40" {...register("description")} />
                    </div>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">User join project</div>
                        <Select
                            mode="tags"
                            style={{
                                width: '100%',
                            }}
                            placeholder="Tags Mode"
                            onChange={handleChange}

                            options={options}
                            defaultValue={defaultValue}
                            className=" w-full outline-none "
                        />
                    </div>
                    <div className="mt-5">
                        <div className="text-gray-500 font-normal">Project Category</div>
                        <select className=" border border-gray-400 rounded-md p-1 w-full outline-none px-2" {...register("category")}>
                            <option value="Software">Software</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Bussiness">Bussiness</option>
                            <option value="Sales">Sales</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div className="gap-2 flex">
                        <button className="border bg-green-600 p-3 rounded-2xl mt-2 text-white text-xl" type="submit">
                            Update
                        </button>
                        <div className="border bg-red-600 p-3 rounded-2xl mt-2 text-white text-xl cursor-pointer"
                            onClick={() => handleDeleteProject()}
                        >
                            Delete Project
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </>)
}

export default SettingProject