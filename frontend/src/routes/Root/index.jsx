import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { checkLogin } from "../../store/Slices/userSlice";
import SideBar from "../../components/SideBar";
export default function Root() {
    const dispatch = useDispatch();
    dispatch(checkLogin());
    const isLogin = useSelector(state => state.user.value);
    const navigate = useNavigate();
    useEffect(() => {
        if (!isLogin) navigate('/Login');
    }, [dispatch, isLogin, navigate])

    return <div className="w-screen h-screen flex">
        <div className="w-[20%] h-full">
            <SideBar></SideBar>
        </div>
        <div className="w-[80%] h-full">
            <Outlet></Outlet>
        </div>
    </div>

}