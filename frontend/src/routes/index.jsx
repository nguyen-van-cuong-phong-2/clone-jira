import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

import Root from './Root';
import Login from '../pages/Login';
import Register from '../pages/Register';
import ErrorPage from "../pages/Error";
import Project from '../pages/Project';
import CreateProject from '../components/CreateProject';
import SettingProject from "../pages/SettingProject";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/Project/:id",
                element: <Project />,

            },
            {
                path: "/CreateProject",
                element: <CreateProject />,
            },
            {
                path: "/SettingProject/:id",
                element: <SettingProject />,
            },
        ]
    },
    {
        path: "/Login",
        element: <Login></Login>,
    },
    {
        path: "/Register",
        element: <Register></Register>,
    },
]);

const App = () => {
    return <RouterProvider router={router} />
}
export default App