import { createSlice } from '@reduxjs/toolkit';
import { getInfoFromToken } from '../../common';
export const userSlice = createSlice({
    name: 'isLogin',
    initialState: { value: false },
    reducers: {
        login: state => {
            state.value = true
        },
        logOut: state => {
            state.value = false
        },
        checkLogin: state => {
            const token = getInfoFromToken();
            if (!token) state.value = false;
            else state.value = true;
        }
    }
})

export const { login, logOut, checkLogin } = userSlice.actions

export default userSlice.reducer
